(function ($) {

    Drupal.behaviors.ekamtheme = {
        attach: function (context, settings) {

            'use strict';

            var adminToolbarHeight = $('#toolbar-administration').height(); // Get admin toolbar height

            $(window).scroll(function () {
                $('.front').toggleClass("sticky bg-dark", ($(window).scrollTop() > (100 + adminToolbarHeight)));
            });

            var header = $('body');
            var menu = $('#sidebar');
            var heightThreshold = $(".sidebar").offset().top;
            var heightThresholdEnd = $(".sidebar").offset().top + $(".sidebar").height();

            $(window).scroll(function () {
                var scroll = $(window).scrollTop();

                if (scroll >= (heightThreshold - adminToolbarHeight) && scroll <= (heightThresholdEnd - adminToolbarHeight)) {
                    header.addClass('fixed-sidebar');
                    menu.addClass('fixed-sidebar');
                } else {
                    header.removeClass('fixed-sidebar');
                    menu.removeClass('fixed-sidebar');
                }
            });

        }
    }

})(jQuery, Drupal);
